package com.wipro.dao;

import com.wipro.entities.HotelRegistrationEntity;

public interface HotelRegistrationRepository {

	public boolean registrationSave(HotelRegistrationEntity hotelRegistrationEntity);
}
