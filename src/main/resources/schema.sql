
create table student
(
   id integer not null,
   name varchar(255) not null,
   passport_number varchar(255) not null,
   primary key(id)
);


CREATE TABLE hotel (
  hotel_id integer  NOT NULL,
  city varchar(45) NOT NULL,
  hotel_name varchar(45) NOT NULL,
  availability_date DATE NOT NULL,
  status varchar(45) NOT NULL,
  room_type varchar(45) NOT NULL,
  price integer,
  gst integer,
  PRIMARY KEY (hotel_id));

CREATE TABLE registration(
  registration_id integer  NOT NULL,
  guest_name varchar(45) NOT NULL,
  gender varchar(45) NOT NULL,
  age integer NOT NULL,  
  email varchar(45) NOT NULL,
  mobile integer NOT NULL, 
  hotel_id integer NOT NULL, 
  PRIMARY KEY (registration_id));
  CONSTRAINT fk_hotel_id FOREIGN KEY (hotel_id)
    REFERENCES hotel(hotel_id)
)