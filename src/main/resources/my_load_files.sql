insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(1,'Hyderabad', 'Ashoka', '2021-9-04', 'Available', 'Delux', 8000, 100);
	
insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(2,'Delhi', 'Ibis', '2021-9-07', 'Available', 'Regular', 2000, 50);	
	
insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(3,'Chennai', 'Ashoka', '2021-9-18', 'Available', 'Delux', 4000, 80);

insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(4,'Bangalore', 'Merrot', '2021-11-04', 'Available', 'Premium', 7000, 100);

insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(5,'Hyderabad', 'Ashoka', '2021-9-04', 'Available', 'Regular', 2000, 40);

insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(6,'Delhi', 'ITC', '2021-11-24', 'Available', 'Delux', 7000, 100);	
	
insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(7,'Chennai', 'Ashoka', '2021-12-04', 'Available', 'Delux', 8000, 100);

insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(8,'Bangalore', 'Ashoka', '2021-11-04', 'Available', 'Delux', 4000, 100);

insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(9,'Hyderabad', 'ITC', '2021-9-04', 'Available', 'Delux', 6000, 40);

insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(10,'Delhi', 'Merrot', '2021-12-04', 'Available', 'Delux', 3000, 70);

insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(11,'Chennai', 'Ashoka', '2021-11-04', 'Available', 'Delux', 5000, 80);	
	
insert into hotel (hotel_id, city, hotel_name,availability_date, status,room_type,price,gst) 
    values(12,'Bangalore', 'Ibis', '2021-10-14', 'Available', 'Delux', 8000, 80);	