package com.wipro.model;

import java.io.Serializable;

public class HotelRegistationModel implements Serializable{
	
	private String guestName;
	
	private String gender;
	
	private Integer age;
	
	private String email;
	
	private String mobile;
	
	private HotelModel hotelDto;

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	
	public HotelModel getHotelDto() {
		return hotelDto;
	}

	public void setHotelDto(HotelModel hotelDto) {
		this.hotelDto = hotelDto;
	}

	@Override
	public String toString() {
		return "HotelRegistationModel [guestName=" + guestName + ", gender=" + gender + ", age=" + age + ", email="
				+ email + ", mobile=" + mobile + "]";
	}
	
	
	

}
