package com.wipro.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.wipro.model.HotelModel;
import com.wipro.entities.HotelEntity;

import org.modelmapper.ModelMapper;

@Repository
public class HotelSearchRepositoryImpl extends BaseDao implements HotelSearchRepository {

	private static final Logger logger = Logger.getLogger(HotelSearchRepositoryImpl.class);

	@Override
	public HotelModel serachAndListHotelDetails(String city, String hotelName, Date availabilityDate) {		
		
		String hql = "FROM HotelEntity h WHERE h.city = :city and h.hotelName = :hotel_name and"
				+ " h.availabilityDate = :availability_date"
				+ " and h.status = :status";
		Query query = getSession().createQuery(hql);
		query.setParameter("city", city);
		query.setParameter("hotel_name", hotelName);
		query.setParameter("availability_date", availabilityDate);
		query.setParameter("status", "Available");

		List<HotelEntity> results = query.list();

		//System.out.println("List : " + results);

		ModelMapper modelMapper = new ModelMapper();
		HotelModel hotelModel = null;

		if (results != null && results.size() > 0) {

			hotelModel = modelMapper.map(results.get(0), HotelModel.class);

			System.out.println("hotelModel : " + hotelModel);
			
		}

		return hotelModel;
	}

	@Override
	public boolean updateHotelStatus(int hotelId, String status) {
		// TODO Auto-generated method stub
		
		System.out.println("Update field:  : Hotel Id : " + hotelId + "  Hotel Status : " + status);
		String hql = "update HotelEntity h set h.status = :status where h.id = :hotel_id";
		Query query = getSession().createQuery(hql);
		query.setParameter("status", status);
		query.setParameter("hotel_id", hotelId);

		int rowCount = query.executeUpdate();

		System.out.println("Rows affected:  : " + rowCount);
		
		if(rowCount == 1)
			return true;
		else
			return false;
	}
	
	

}
