package com.wipro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.dao.HotelRegistrationRepository;
import com.wipro.entities.HotelRegistrationEntity;

@Service
public class HotelRegistationService {
	

	@Autowired
	HotelRegistrationRepository hotelRegistrationRepository;
	
	public boolean registrationSave(String guestName, String gender, String age,
			String email, String mobile) {
		
		HotelRegistrationEntity hotelRegistrationEntity = new HotelRegistrationEntity();
		hotelRegistrationEntity.setGuestName(guestName);
		hotelRegistrationEntity.setAge(Integer.parseInt(age));
		hotelRegistrationEntity.setGender(gender);
		hotelRegistrationEntity.setEmail(email);
		hotelRegistrationEntity.setMobile(mobile);
		
		hotelRegistrationRepository.registrationSave(hotelRegistrationEntity);			
		
		return true;
	}
}
