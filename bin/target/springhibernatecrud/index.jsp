<html>
<body >
<br/><br/>
<center>
    <header style="font-size:30px">Hotel Reservation System </header>
    <br/><br/>
    <form  action="/springhibernatecrud/search" method="GET" id="search_hotel">
        <table>
            <tr>
                <td style="font-size:25px">Search Hotels</td>
                <td></td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><label style="font-size:20px">City :</label></td>
                <td>
                    <select name="city" id="city">
                        <option value="Bangalore">Bangalore</option>
                        <option value="Chennai">Chennai</option>
                        <option value="Delhi">Delhi</option>
                        <option value="Hyderabad">Hyderabad</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><label style="font-size:20px">Hotel :</label></td>
                <td>
                    <select name="hotel" id="hotel">
                        <option value="Ashoka">The Ashoka</option>
                        <option value="Ibis">Ibis</option>
                        <option value="Merrot">Merrot</option>
                        <option value="ITC">ITC Gardenia</option>
                         <option value="OYO">OYO</option>
                        <option value="Oberoi">Oberoi</option>
                        <option value="Grand">Grand Hotel</option>
                        <option value="LeelaPalace">Leela Palace</option>
                         <option value="Taj">Hotel Taj</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><label style="font-size:20px">Date :</label></td>
                <td>
                    <input type="date" id = "date" name="date">
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <button style="font-size:20px">Check Availability</button>
                </td>
            </tr>
        </table>
    </form>
</center>
</body>
</html>
