<html>
<title> Hotel Search Failure Page</title>
  <body style="color:red; font-size: 20px; font-weight: bold; align-content: center;">
  <br/><br/>
     <p style="text-align:center;">Sorry, this hotel is not available for the chosen date. 
     Please click on the below link 
     to choose another hotel
     <br/>
     <br/>
     <a href = "/springhibernatecrud">Choose Again</a>
     
     <h2>Result : ${search_response}</h2>
  </body>

</html>