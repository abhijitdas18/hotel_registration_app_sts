package com.wipro.dao;

import java.util.Date;

import com.wipro.entities.HotelEntity;
import com.wipro.model.HotelModel;

public interface HotelSearchRepository {
	
	HotelModel serachAndListHotelDetails(String city, String hotel, Date date);
	
	boolean updateHotelStatus(int hotelId, String status);

}
