package com.wipro.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.dao.HotelRegistrationRepository;
import com.wipro.dao.HotelSearchRepository;
import com.wipro.model.HotelModel;

@Service
public class HotelSearchService  {
	
	@Autowired
	HotelSearchRepository hotelSearchRepository;
	
	@Autowired
	HotelRegistrationRepository registrationRepository;

	public HotelModel serachAndListHotelDetails(String city, String hotel, String date) {
	
		Date dateObj = null;
		try {
			
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			dateObj = (Date)formatter.parse(date);			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		return hotelSearchRepository.serachAndListHotelDetails(city, hotel, dateObj);
		
	}

	public void formSearchResponse(HotelModel hotelDTO, List<String> searchResults) {
		
		searchResults.add(hotelDTO.getRoomType());
		searchResults.add(Integer.toString(hotelDTO.getPrice()));
		searchResults.add(Integer.toString(hotelDTO.getGst()));
		searchResults.add(Integer.toString(hotelDTO.getPrice() + hotelDTO.getGst()));
		
	}

	public boolean updateHotelStatus(int hotelId, String status) {
		
		hotelSearchRepository.updateHotelStatus(hotelId, status);
		return true;
	}
}
