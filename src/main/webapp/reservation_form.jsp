<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>

<style>
table, td, th {
	text-align: center;
}

table {
	border-collapse: collapse;
	width: 60%;
}

th, td {
	padding: 15px;
}
</style>
</head>
<body style="text-align: center">

	<script type="text/javascript">
		function reserve() {

			alert("confirm is clicked :");

			var hotelName = "<c:out value='${selectedHotel}' />";

			alert("confirm is clicked :" + hotelName);
			document.forms[0].action = "/springhibernatecrud/reserve?selectedHotel="
					+ hotelName;
			document.forms[0].method = "post"; // "get"
			document.forms[0].submit();
			//alert(document.getElementById("confirm"));
		}
	</script>

	<br />
	<br />


<%-- 	<header style="font-size: 30px">Reservation Form for
		${selectedHotel}</header> --%>
	<br />
	<br />
	<form action="/springhibernatecrud/reserve" method="POST"
		id="reserve_hotel">
		<!-- <form>	 -->
		<table border=1
			style="text-align: center; margin-left: auto; margin-right: auto;width:40%;">
			<tr>
				<td style="text-align: center;">
					<table  style="margin-left: auto; margin-right: auto;">
					
						<tr>
								<td colspan="2" style="font-size: 25px; text-align: left; text-decoration: underline; font-weight: bold">Reservation Form</td>								
						</tr>
						<tr>
							<td style="text-align: left;"><label style="font-size: 20px;">Guest Name </label></td>
							<td style="text-align: left;"><input type="text" name="guest" id="guest" /></td>
						</tr>
						<tr>
							<td style="text-align: left;"><label style="font-size: 20px">Gender </label></td>
							<td style="text-align: left;"><select name="gender" id="gender" >
									<option value="male">Male</option>
									<option value="female">Female</option>
									<option value="others">Others</option>
							</select></td>
						</tr>
						<tr>
							<td style="text-align: left;"><label style="font-size: 20px">Age </label></td>
							<td style="text-align: left;"><input type="text" name="age" id="age" size="20" value="0" /></td>
						</tr>
						<tr>
							<td style="text-align: left;"><label style="font-size: 20px">Email Id</label></td>
							<td style="text-align: left;"><input type="text" name="email" id="email" /></td>
						</tr>
						<tr>
							<td style="text-align: left;"><label style="font-size: 20px">Mobile No.</label></td>
							<td style="text-align: left;"><input type="text" name="mobile" id="mobile" /></td>
						</tr>
						<tr>
							<td colspan="2" style="text-align: left; padding-left: 130px;">								
								<button style="font-size: 20px; ">Reserve</button> 
							</td>							
						</tr>


					</table>

				</td>
			</tr>
		</table>

	</form>

</body>
</html>