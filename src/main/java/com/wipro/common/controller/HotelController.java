package com.wipro.common.controller;


import org.springframework.web.bind.annotation.RequestParam;

import com.wipro.model.HotelModel;
import com.wipro.services.HotelRegistationService;
import com.wipro.services.HotelSearchService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.ModelMap;



@Controller
public class HotelController {

	@Autowired
	private HotelSearchService hotelSearchService;
	
	@Autowired
	private HotelRegistationService hotelRegistrationService;
	
  
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String sayHello(@RequestParam(value = "myName", defaultValue = "World") String name) {
        return String.format("Hello %s!", name);
    }
  
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(@RequestParam (value="city", defaultValue = "") String city,
                         @RequestParam (value="hotel", defaultValue = "") String hotel,
                         @RequestParam (value="date", defaultValue = "") String date,
                         Model model, HttpServletRequest request){
        System.out.println("HotelController : Search Page : City: " + city + " ,Hotel : " + hotel + ", Date : " + date); 
        
        if("".equals(date) || date == null) {
       	 System.out.println("Search Page: Date can not be empty."); 
       	 return String.format("search_failure");	
        }
        
        HotelModel hotelModel = hotelSearchService.serachAndListHotelDetails(city, hotel, date); 
         
        
         
        
        
        if(hotelModel != null) {
        	List<String> searchResults = new ArrayList<>();
        	hotelSearchService.formSearchResponse(hotelModel, searchResults);    
        	model.addAttribute("search_response", searchResults);
        	request.getSession().setAttribute("selectedHotel", hotelModel.getHotelName());
        	request.getSession().setAttribute("hotelModel", hotelModel);
        	return String.format("search_success");
        }else {
        	System.out.println("Search Page: Either search criterias not matched or table is empty.");
        	 return String.format("search_failure");	
        }       
       
    }
    
    
    @RequestMapping(value = "/reserve", method = RequestMethod.POST)
    public String reserve(@RequestParam (value="guest", defaultValue = "") String guestName,
                         @RequestParam (value="gender", defaultValue = "") String gender,
                         @RequestParam (value="age", defaultValue = "") String age,
                         @RequestParam (value="email", defaultValue = "") String email,
                         @RequestParam (value="mobile", defaultValue = "") String mobile,
                         Model model, HttpServletRequest request){
    	
        System.out.println("HotelController : reserve : Guest Name : " + guestName + " ,Gender : " + gender + " , Age : " + age+ ""
        		+ " , Email : " + email + " , Mobile : "  + mobile); 
        String selectedHotel = (String) request.getSession().getAttribute("selectedHotel");
        HotelModel hotelModel = (HotelModel)request.getSession().getAttribute("hotelModel");
        System.out.println("HotelController : reserve : selectedHotel : " + selectedHotel);
        
        
        hotelRegistrationService.registrationSave(guestName, gender, age, email, mobile);
        hotelSearchService.updateHotelStatus(hotelModel.getId(), "Full");
        
        model.addAttribute("guestName", guestName);        
        return "reservation_confirmation";
       
    }
}
