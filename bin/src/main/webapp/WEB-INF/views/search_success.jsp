<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<title></title>
<body style="text-align: center">
	<br />
	<br />


	<script type="text/javascript">
		document.getElementById("myButton").onclick = function() {
			location.href = "www.yoursite.com";
		};

		function myBackFunction() {
			//alert("back is clicked");
			document.forms[0].action = "/springhibernatecrud/index.jsp";
			document.forms[0].method = "get"; // "get"
			document.forms[0].submit();
			//alert(document.getElementById("back"));
		}

		function myConfirmFunction() {
			
			var hotelName = "<c:out value='${selectedHotel}' />";		
		
			alert("confirm is clicked :" + hotelName);
			document.forms[0].action = "/springhibernatecrud/reservation_form.jsp";
			document.forms[0].method = "get"; // "get"
			document.forms[0].submit();
			//alert(document.getElementById("confirm"));
		}
	</script>
	
	<%
	   String hotel = request.getParameter("selectedHotel");
	   System.out.println("hotel:::::: "  + hotel);
	   session.setAttribute("hotelName",hotel);

     %>



	<!-- <form  action="/springhibernatecrud/reservation_form.jsp" method="GET" id="reservation_form"> -->
	<form action="" onsubmit="myFunction()"></form>
	<c:if test="${not empty search_response}">

		<table border="1">
			<tr>
				<td>Room Type</td>
				<td>Price</td>
				<td>GST</td>
				<td>Total</td>
			</tr>
			<tr>
				<c:forEach var="listValue" items="${search_response}">
					<td>${listValue}</td>
				</c:forEach>
			</tr>

			<tr>
				<td>
				  <input id="hotelName" name="hotelName" type="hidden" value="${selectedHotel}">
					<button style="font-size: 20px" id="confirm"
						onclick="myConfirmFunction()">CONFIRM</button>
				</td>
				<td>
					<button style="font-size: 20px" id="back"
						onclick="myBackFunction()">BACK</button>
				</td>
			</tr>

		</table>

	</c:if>
	</form>

</body>
</html>