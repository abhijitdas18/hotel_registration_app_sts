package com.wipro.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


// @JoinColumn annotation to configure the name of the column in the table
// that maps to the primary key in the HotelEntity table
@Entity
@Table(name = "registration")
public class HotelRegistrationEntity {
	
	@Id
	@Column(name = "registration_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "guest_name")
	private String guestName;

	@Column(name = "gender")
	String gender;
	
	@Column(name = "age")
	private Integer age;
	
	@Column(name = "email")
	private String email;	
	
	@Column(name = "mobile")
	private String mobile;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "hotel_id", referencedColumnName = "hotel_id")
    private HotelEntity hotelEntity;
	

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	

	public HotelEntity getHotelEntity() {
		return hotelEntity;
	}

	public void setHotelEntity(HotelEntity hotelEntity) {
		this.hotelEntity = hotelEntity;
	}

	@Override
	public String toString() {
		return "HotelRegistationEntity [guestName=" + guestName + ", gender=" + gender + ", age=" + age + ", email="
				+ email + ", mobile=" + mobile + "]";
	}
	
	
	

}
