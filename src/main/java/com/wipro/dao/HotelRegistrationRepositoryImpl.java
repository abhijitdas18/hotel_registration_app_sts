package com.wipro.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.wipro.entities.HotelRegistrationEntity;

@Repository
public class HotelRegistrationRepositoryImpl extends BaseDao implements HotelRegistrationRepository{

	@Override
	public boolean registrationSave(HotelRegistrationEntity hotelRegistrationEntity) {
		
		System.out.println("hotelRegistrationEntity : Saving the Registration Data :  " + hotelRegistrationEntity);
		Session session = getSession();
		session.save(hotelRegistrationEntity);
		session.close();	
		return true;
	}
}
