<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Script in head section</title>

<style>
table, td, th {
	text-align: center;
}

table {
	border-collapse: collapse;
	width: 60%;
}

th, td {
	padding: 15px;
}
</style>
</head>

<body onload="myFunction()" style="text-align: center">
	<script>
 function myFunction() {
	 
	  document.getElementById('date').min = new Date(new Date().getTime() - 
			  new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];

	}
	</script>

	<br />
	<br />

	<header style="font-size: 30px">Hotel Reservation System </header>
	<br />
	<br />

	<form action="/springhibernatecrud/search" method="GET"
		id="search_hotel">
		<!-- <div style="text-align: center"> -->
		<table border=1
			style="text-align: center; margin-left: auto; margin-right: auto; width: 40%;">
			<tr>
				<td style="text-align: center;">
					<table
						style="text-align: center; margin-left: auto; margin-right: auto;">
						<tr>
							<td colspan="2" style="font-size: 25px; text-align: left;">Search
								Hotels</td>
						</tr>

						<tr>
							<th><label style="font-size: 20px">City :</label> </h>
							<td style="text-align: left;"><select name="city" id="city">
									<option value="Bangalore">Bangalore</option>
									<option value="Chennai">Chennai</option>
									<option value="Delhi">Delhi</option>
									<option value="Hyderabad">Hyderabad</option>
							</select></td>
						</tr>

						<tr>
							<th><label style="font-size: 20px">Hotel :</label></th>
							<td style="text-align: left;"><select name="hotel"
								id="hotel">
									<option value="Ashoka">The Ashoka</option>
									<option value="Ibis">Ibis</option>
									<option value="Merrot">Merrot</option>
									<option value="ITC">ITC Gardenia</option>
									<option value="OYO">OYO Group</option>
									<option value="Oberoi">Oberoi Group</option>
									<option value="Grand">Grand Hotel</option>
									<option value="LeelaPalace">Leela Palace</option>
									<option value="Taj">Hotel Taj</option>
							</select></td>
						</tr>

						<tr>
							<th><label style="font-size: 20px">Date :</label> </h>
							<td style="text-align: left;"><input type="date" min='1899-01-01' id="date" name = "date"/></td>
						</tr>

						<tr>
							<td colspan="2" style="text-align: left; padding-left: 70px;">
								<button style="font-size: 20px">Check Availability</button>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- 	</div> -->
	</form>

</body>
</html>
