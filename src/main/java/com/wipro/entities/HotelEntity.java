package com.wipro.entities;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

// One hotel can be booked from many customers ( many registrations)
//One customer can book one hotel
//The address side of the relationship is called the non-owning side. 
//bidirectional relationship
@Entity
@Table(name = "hotel")
public class HotelEntity {
	
	@Id
	@Column(name = "hotel_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "hotel_name")
	private String hotelName;
	
	@Column(name = "availability_date")
	private Date availabilityDate;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "room_type")
	private String roomType;
	
	@Column(name = "price")
	private Integer price;
	
	@Column(name = "gst")
	private Integer gst;
	
	@OneToOne(mappedBy = "hotelEntity")
	private HotelRegistrationEntity hotelRegistrationEntity;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public Date getAvailabilityDate() {
		return availabilityDate;
	}
	public void setAvailabilityDate(Date availabilityDate) {
		this.availabilityDate = availabilityDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getGst() {
		return gst;
	}
	public void setGst(Integer gst) {
		this.gst = gst;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public HotelRegistrationEntity getHotelRegistrationEntity() {
		return hotelRegistrationEntity;
	}
	public void setHotelRegistrationEntity(HotelRegistrationEntity hotelRegistrationEntity) {
		this.hotelRegistrationEntity = hotelRegistrationEntity;
	}
	@Override
	public String toString() {
		return "HotelEntity [city=" + city + ", hotelName=" + hotelName + ", availabilityDate=" + availabilityDate
				+ ", status=" + status + ", roomType=" + roomType + ", price=" + price + ", gst=" + gst
				+ ", hotelRegistrationEntity=" + hotelRegistrationEntity + "]";
	}
			

}
